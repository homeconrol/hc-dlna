<?
    
//require_once __DIR__ . "/../libs/HC_UPNP_Discovery_Device.php"; 


	
	// Klassendefinition
    class HCDLNA extends IPSModule {
        /**
        * Die folgenden Funktionen stehen automatisch zur Verfügung, wenn das Modul über die "Module Control" eingefügt wurden.
        * Die Funktionen werden, mit dem selbst eingerichteten Prefix, in PHP und JSON-RPC wiefolgt zur Verfügung gestellt:
        *
        * ABC_MeineErsteEigeneFunktion($id);
        *
        */
        public function MeineErsteEigeneFunktion() {
            echo $this->InstanceID;
        }
		
		public function Create() {
			//Never delete this line!
			parent::Create();
			
			$this->RegisterPropertyString("LejatszokID", "");
		}
		
		
		public function FindPlayer() {
			
			require __DIR__ . '/../libs/HC_UPNP_Discovery_Device.php';
			
			
			////PROFIL KERES
			if (!IPS_VariableProfileExists("PLAYER"))
			{
				
				IPS_CreateVariableProfile ("PLAYER", 1);
				IPS_SetVariableProfileIcon ("PLAYER", "Speaker");
				IPS_SetVariableProfileAssociation("PLAYER", 1, "KIVÁLASZT", "Speaker", 0xFFFFFF);
			}
				
			
			
			$CategoryID = $this->ReadPropertyString('LejatszokID');
			echo $CategoryID;
										
			$ST = "urn:schemas-upnp-org:device:MediaRenderer:1";
			$DeviceArray = mSearch($ST);
			
			//print_r ($DeviceArray);
			
			
			for($i=0,$size=count($DeviceArray);$i<$size;$i++)
			{
				$DeviceDescription = $DeviceArray[$i]['LOCATION'];
				$vars1 = explode("//", $DeviceDescription);	//cut nach http
				$cutted1 	= $vars1[0];
				$cutted2 	= $vars1[1];
				$vars2 = explode("/", $cutted2);					//cut nach Port
				$cutted3 	= $vars2[0];
				$cutted4 	= $vars2[1];
				$vars3 = explode(":", $cutted3);					//IP und Port
				$DeviceIP 	= $vars3[0];
				$DevicePort = $vars3[1];

				$root = "http://"."$cutted3"."/";

				//Description von Gerät abrufen----------------------------------------------

				$xml = file_get_contents("$DeviceDescription", -1);

				$xmldesc = new SimpleXMLElement($xml);
				$modelName = $xmldesc->device->modelName;
				$UDN = $xmldesc->device->UDN;
				$friendlyName = $xmldesc->device->friendlyName;

					if (isset ($xmldesc->device->serviceList->service->serviceType) == "urn:schemas-upnp-org:service:AVTransport:1")
					{
						//verfügbare Services-----------------------------------------------------

						if (isset ($xmldesc->device->serviceList->service))
						{
							$services = array();

							foreach($xmldesc->device->serviceList->service as $val1)
							{
								$type = ($val1->serviceType);
								$services["$type"] = $val1;

								$service = array();
							
							foreach($val1 as $val2)
							{
								$service['serviceType'] = $val2->serviceType;
								$service['SCPDURL'] = $val2->SCPDURL;
								$service['controlURL'] = $val2->controlURL;
							}
							}
			
						}

		//ControlURL auslesen (urn:schemas-upnp-org:service:AVTransport:1)--------------

					if (isset($services['urn:schemas-upnp-org:service:AVTransport:1']))
					{
						$Directory = ($services['urn:schemas-upnp-org:service:AVTransport:1']->controlURL);
					}
					else
					{
						$Directory = "MediaRenderer/AVTransport/Control";
			
					}

					//da manchmal "/" davor und manchmal nicht
					if ( 0 === strpos($Directory, "/") )//prüfen, ob erstes Zeichen ein "/" ist
					{
						$raw_controlURL = trim( end($Directory), "/" );
					}
					else
					{
						$raw_controlURL = $Directory;
					}
					$controlURL = ("/".$raw_controlURL);

					
					if (isset($services['urn:schemas-upnp-org:service:RenderingControl:1']))
					{
						$Directory = ($services['urn:schemas-upnp-org:service:RenderingControl:1']->controlURL);
					}
					else
					{
						$Directory = "MediaRenderer/RenderingControl/Control";
					}

					//da manchmal "/" davor und manchmal nicht
					if ( 0 === strpos($Directory, "/") )//prüfen, ob erstes Zeichen ein "/" ist
					{
						$raw_renderingcontrolURL = trim( end($Directory), "/" );
					}
					else
					{
						$raw_renderingcontrolURL = $Directory;
					}
				 $renderingcontrolURL = ("/".$raw_renderingcontrolURL);
					}


		
			$VarID_Device = IPS_CreateVariable(1);
			IPS_SetName ($VarID_Device , "$friendlyName");
			IPS_SetVariableCustomProfile ($VarID_Device , "PLAYER");
			//IPS_SetVariableCustomAction ($VarID_Device , 34523 /*[UPNP AUDIO\SELECTED PLAYER]*/ );
			IPS_SetParent ($VarID_Device ,  $CategoryID);
			
			
    			$DEVIP = IPS_CreateVariable(3);
    			IPS_SetName ($DEVIP , "DeviceIP");
    			IPS_SetParent ($DEVIP , $VarID_Device);
    			IPS_SetHidden ($DEVIP , true);
    			SetValue($DEVIP,"$DeviceIP");

    			$DEVPORT = IPS_CreateVariable(3);
    			IPS_SetName ($DEVPORT , "DevicePort");
    			IPS_SetParent ($DEVPORT , $VarID_Device);
    			IPS_SetHidden ($DEVPORT , true);
    			SetValue($DEVPORT,"$DevicePort");

    			$DEVCURL = IPS_CreateVariable(3);
    			IPS_SetName ($DEVCURL , "controlURL");
    			IPS_SetParent ($DEVCURL , $VarID_Device);
    			IPS_SetHidden ($DEVCURL , true);
    			SetValue($DEVCURL,"$controlURL");

    			$DEVCRL = IPS_CreateVariable(3);
    			IPS_SetName ($DEVCRL , "renderingcontrolURL");
    			IPS_SetParent ($DEVCRL , $VarID_Device);
    			IPS_SetHidden ($DEVCRL , true);
    			SetValue($DEVCRL,"$renderingcontrolURL");

		

		}

			
			
		} ///// FindPlayer() VEGE
		
		
    }
?>