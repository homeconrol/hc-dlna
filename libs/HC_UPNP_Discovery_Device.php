<?
##### Project Exporter Comment: Script Version Stand 18.11.2013 11:50 #####


##### Project Exporter Comment: Script Version Stand 16.08.2013 22:09 #####



//------------------------------------------------------------------------------
//UPNP_Discovery_Functions.ips.php----------------------------------------------
//UPNP_Discovery_Functions_V1.2-------------------------------------------------
//05.08.2013--------------------------------------------------------------------
//------------------------------------------------------------------------------
//mögliche Aufrufe:-------------------------------------------------------------
//$ST_ALL = "ssdp:all";
//$ST_RD = "upnp:rootdevice";
//$ST_AV = "urn:dial-multiscreen-org:service:dial:1";
//$ST_MS = "urn:schemas-upnp-org:device:MediaServer:1";
//$ST_CD = "urn:schemas-upnp-org:service:ContentDirectory:1";
//$ST_RC = "urn:schemas-upnp-org:service:RenderingControl:1";
//$ST_MR = "urn:schemas-upnp-org:device:MediaRenderer:1";
//------------------------------------------------------------------------------
//Test:-------------------------------------------------------------------------
//$ST = $ST_ALL;
//print_r (mSearch($ST));
//------------------------------------------------------------------------------

//$ST = $ST_MR;
//print_r (mSearch($ST));



function mSearch($ST)
	{
	//Variablen------------------------------------------------------------------
	$USER_AGENT = 'MacOSX/10.8.2 UPnP/1.1 PHP-UPnP/0.0.1a';
	$MULTICASTIP = '239.255.255.250';
	$MX = 3;
	$MAN = 'ssdp:discover';
	$sockTimout = '10';

	//Message--------------------------------------------------------------------
	$msg  = 'M-SEARCH * HTTP/1.1' . "\r\n";
	$msg .= 'HOST: '.$MULTICASTIP.':1900' . "\r\n";
	$msg .= 'MAN: "'. $MAN .'"' . "\r\n";
	$msg .= 'MX: '. $MX ."\r\n";
	$msg .= 'ST: ' . $ST ."\r\n";
	$msg .= 'USER-AGENT: '. $USER_AGENT . "\r\n";
	$msg .= '' ."\r\n";




	//MULTICAST MESSAGE absetzen-------------------------------------------------
	$sock = socket_create( AF_INET, SOCK_DGRAM, getprotobyname('udp') );
	$opt_ret = socket_set_option( $sock, SOL_SOCKET, SO_REUSEADDR, 1 );
	$send_ret = socket_sendto( $sock, $msg, strlen( $msg ), 0, $MULTICASTIP, 1900);

	//TIMEOUT setzen-------------------------------------------------------------
	socket_set_option( $sock, SOL_SOCKET, SO_RCVTIMEO, array( 'sec'=>$sockTimout, 'usec'=>'0' ) );

	//RESPONSE empfangen und bearbeiten (parseMSearchResponse())-----------------
	$response = array();
	do
	{
		unset( $buf );
		@socket_recv( $sock, $buf, 1024, MSG_WAITALL );
		if( !is_null($buf) ) $response[] = parseMSearchResponse( $buf );
		
	}

	while( !is_null($buf) );

	//SOCKET schliessen----------------------------------------------------------
	socket_close( $sock );

	return $response;
	//print_r ($response);
}

function parseMSearchResponse( $response )
	{
		$responseArray = explode( "\r\n", $response );

		$parsedResponse = array();

		//Response auslesen und bearbeiten, da häufig unterschiedlich in Groß- und Kleinschreibung sowie Leerzeichen dazwischen
		foreach( $responseArray as $row )
			{
			if( stripos( $row, 'HTTP' ) === 0 ){
				$parsedResponse['HTTP'] = $row;
				}
			if( stripos( $row, 'CACHE-CONTROL:' ) === 0 )
				{
				$parse = str_ireplace( 'CACHE-CONTROL:', '', $row );

				$parsedResponse['CACHE-CONTROL'] = str_ireplace( 'CACHE-CONTROL:', '', $row );

				//prüfen, ob erstes Zeichen ein "Leerzeichen" ist, da
				if ( 0 === strpos($parse, " ") )
					{
					$parsedResponse['CACHE-CONTROL'] = trim( $parse, " " );

				}else{
						$parsedResponse['CACHE-CONTROL'] = $parse;
						}
				}
			if( stripos( $row, 'DATE:') === 0 )
				{
				$parse = str_ireplace( 'DATE:', '', $row );

				$parsedResponse['DATE'] = str_ireplace( 'DATE:', '', $row );

				//prüfen, ob erstes Zeichen ein "Leerzeichen" ist, da
				if ( 0 === strpos($parse, " ") )
					{
					$parsedResponse['DATE'] = trim( $parse, " " );

				}else{
						$parsedResponse['DATE'] = $parse;
						}
				}
			if( stripos( $row, 'LOCATION:') === 0 )
				{
				$parse = str_ireplace( 'LOCATION:', '', $row );

				$parsedResponse['LOCATION'] = str_ireplace( 'LOCATION:', '', $row );

				//prüfen, ob erstes Zeichen ein "Leerzeichen" ist, da
				if ( 0 === strpos($parse, " ") )
					{
					$parsedResponse['LOCATION'] = trim( $parse, " " );

				}else{
						$parsedResponse['LOCATION'] = $parse;
						}
				}
			if( stripos( $row, 'SERVER:') === 0 )
				{
				$parse = str_ireplace( 'SERVER:', '', $row );

				$parsedResponse['SERVER'] = str_ireplace( 'SERVER:', '', $row );

				//prüfen, ob erstes Zeichen ein "Leerzeichen" ist, da
				if ( 0 === strpos($parse, " ") )
					{
					$parsedResponse['SERVER'] = trim( $parse, " " );

				}else{
						$parsedResponse['SERVER'] = $parse;
						}
				}
			if( stripos( $row, 'ST:') === 0 )
				{
				$parse = str_ireplace( 'ST:', '', $row );

				$parsedResponse['ST'] = str_ireplace( 'ST:', '', $row );

				//prüfen, ob erstes Zeichen ein "Leerzeichen" ist, da
				if ( 0 === strpos($parse, " ") )
					{
					$parsedResponse['ST'] = trim( $parse, " " );

				}else{
						$parsedResponse['ST'] = $parse;
						}
				}
			if( stripos( $row, 'USN:') === 0 )
				{
				$parse = str_ireplace( 'USN:', '', $row );

				$parsedResponse['USN'] = str_ireplace( 'USN:', '', $row );

				//prüfen, ob erstes Zeichen ein "Leerzeichen" ist, da
				if ( 0 === strpos($parse, " ") )
					{
					$parsedResponse['USN'] = trim( $parse, " " );

				}else{
						$parsedResponse['USN'] = $parse;
						}
				}
		}
		return $parsedResponse;
	}

function array_multi_unique($multiArray)
{
	$uniqueArray = array();

	foreach($multiArray as $subArray) //alle Array-Elemente durchgehen
		{
		if(!in_array($subArray, $uniqueArray)) //prüfen, ob Element bereits im Unique-Array
			{
			$uniqueArray[] = $subArray; //Element hinzufügen, wenn noch nicht drin
			}
		}
	return $uniqueArray;
}

?>